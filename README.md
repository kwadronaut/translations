This repository holds all translations, with one branch per project.
For more information, please read:

https://gitlab.torproject.org/tpo/community/l10n/-/wikis/home

This are the translations for TorVPN.

More information: 
https://gitlab.torproject.org/tpo/applications/vpn/

The original file can be located at https://gitlab.torproject.org/tpo/applications/vpn/-/raw/main/app/src/main/res/values/strings.xml
